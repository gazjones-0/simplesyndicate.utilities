# README #

SimpleSyndicate.TfsUtil

### What is this repository for? ###

* Utilities for working with Team Foundation Server.

### How do I get started? ###

* Build the application, and use it in your own projects

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.utilities/issues?status=new&status=open

### How do I use it? ###

```
#!text
Usage: SimpleSyndicate.TfsUtil <command> [<args>]

Command summary:
get                          Gets latest on every workspace
version                      Outputs TfsUtil version

Command details:
get
  Does a get latest for every workspace.

version
  Outputs the version of SimpleSyndicate.TfsUtil.
```
