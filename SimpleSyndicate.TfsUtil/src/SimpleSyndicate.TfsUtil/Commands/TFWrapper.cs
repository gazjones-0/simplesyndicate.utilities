﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace SimpleSyndicate.TfsUtil.Commands
{
    /// <summary>
    /// Wrapper for <c>tf.exe</c>.
    /// </summary>
    public class TFWrapper
    {
        /// <summary>
        /// The standard output from <c>tf.exe</c>.
        /// </summary>
        private readonly string _stdout = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="TFWrapper"/> class.
        /// </summary>
        /// <param name="stdout">The standard output from running <c>tf.exe</c>.</param>
        private TFWrapper(string stdout) => _stdout = stdout;

        /// <summary>
        /// Gets the current output as a list, with each line in the output being a separate item in the list.
        /// </summary>
        /// <value>Current output.</value>
        public IList<string> AsList => _stdout.Split(new string[] { "\r\n" }, StringSplitOptions.None).ToList();

        /// <summary>
        /// Gets the current output as a string.
        /// </summary>
        /// <value>Current output.</value>
        public string AsString => _stdout;

        /// <summary>
        /// Executes <c>tf.exe</c> using the specified <paramref name="arguments"/>.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <returns>A <see cref="TFWrapper"/> instance.</returns>
        public static TFWrapper Execute(string arguments) => Execute(arguments, null);

        /// <summary>
        /// Executes <c>tf.exe</c> using the specified <paramref name="arguments"/>, and in the specified <paramref name="workingDirectory"/>.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="workingDirectory">The working directory.</param>
        /// <returns>A <see cref="TFWrapper"/> instance.</returns>
        public static TFWrapper Execute(string arguments, string workingDirectory) => ExecuteTf(arguments, workingDirectory);

        /// <summary>
        /// Executes <c>tf.exe</c> using the specified <paramref name="arguments"/>, and in the specified <paramref name="workingDirectory"/>.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="workingDirectory">The working directory.</param>
        /// <returns>A <see cref="TFWrapper"/> instance.</returns>
        private static TFWrapper ExecuteTf(string arguments, string workingDirectory)
        {
            if (ExecuteTfCheck())
            {
                return ExecuteOperation.Execute(() => ExecuteTfOperation(arguments, workingDirectory));
            }

            return new TFWrapper(string.Empty);
        }

        /// <summary>
        /// Returns whether <c>tf.exe</c> can be started.
        /// </summary>
        /// <returns>Whether the application can be started</returns>
        private static bool ExecuteTfCheck()
        {
            using (var process = new Process())
            {
                process.StartInfo.WorkingDirectory = Environment.CurrentDirectory;
                process.StartInfo.FileName = "tf";
                process.StartInfo.Arguments = string.Empty;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                try
                {
                    process.Start();
                    process.WaitForExit();
                    return true;
                }
                catch (Exception e)
                {
                    Program.OutputError("Caught exception whilst checking for tf.exe:\r\n" + e.Message);
                }
            }

            return false;
        }

        /// <summary>
        /// Executes <c>tf.exe</c> with the specified <paramref name="arguments"/>, and in the specified <paramref name="workingDirectory"/>.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="workingDirectory">The working directory; if <c>null</c> the current directory will be used.</param>
        /// <returns>A <see cref="TFWrapper"/> instance.</returns>
        private static TFWrapper ExecuteTfOperation(string arguments, string workingDirectory)
        {
            using (var process = new Process())
            {
                process.StartInfo.FileName = "tf";
                process.StartInfo.Arguments = string.Empty;
                process.StartInfo.WorkingDirectory = workingDirectory ?? Environment.CurrentDirectory;
                process.StartInfo.CreateNoWindow = false;
                process.StartInfo.UseShellExecute = false;
                if (!string.IsNullOrWhiteSpace(arguments))
                {
                    process.StartInfo.Arguments = arguments;

                    // get requires special handling, as if we attempt to redirect standard output it won't show the
                    // resolve conflict window; for this situation we don't do any redirection
                    if (arguments.StartsWith("get", StringComparison.OrdinalIgnoreCase))
                    {
                        try
                        {
                            process.Start();
                            process.WaitForExit();
                            return new TFWrapper(string.Empty);
                        }
                        catch (Exception e)
                        {
                            Program.OutputError("Caught exception whilst executing operation\r\n" + e.ToString());
                            throw;
                        }
                    }
                }

                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                string stdout = null;
                try
                {
                    process.Start();
                    stdout = process.StandardOutput.ReadToEnd();
                    process.WaitForExit();
                    return new TFWrapper(stdout);
                }
                catch (Exception e)
                {
                    Program.OutputError("Caught exception whilst executing operation\r\n" + e.ToString());
                    throw;
                }
            }
        }
    }
}
