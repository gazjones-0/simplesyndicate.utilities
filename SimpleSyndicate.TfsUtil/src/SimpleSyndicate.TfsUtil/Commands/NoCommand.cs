﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.Linq;

namespace SimpleSyndicate.TfsUtil.Commands
{
    /// <summary>
    /// No command.
    /// </summary>
    public static class NoCommand
    {
        /// <summary>
        /// Executes the "no command" functionality if no command has been specified.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Execute(string[] args)
        {
            if (args.Count() == 0)
            {
                Program.OutputUsage();
                Program.CommandExecuted();
            }
        }
    }
}
