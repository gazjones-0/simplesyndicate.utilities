﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

namespace SimpleSyndicate.TfsUtil.Commands.Help
{
    /// <summary>
    /// Help command.
    /// </summary>
    public static class HelpCommand
    {
        /// <summary>
        /// Executes the help command if the arguments specify help as the command.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Execute(string[] args)
        {
            if (CommandUtility.IsCommand(args, "Help"))
            {
                Program.CommandExecuted();
                Program.OutputUsage();
            }
        }
    }
}
