﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SimpleSyndicate.TfsUtil.Commands
{
    /// <summary>
    /// Client for working with TFS, using the existing connections the current user has.
    /// </summary>
    public static class TfsClient
    {
        /// <summary>
        /// Returns whether there's an existing connection for a TFS server.
        /// </summary>
        /// <returns>Whether there's an existing connection for a TFS server.</returns>
        public static bool Exists()
        {
            if (TFWrapper.Execute("connections").AsString.StartsWith("Server", StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Does a get latest on the specified <paramref name="workspace"/>.
        /// </summary>
        /// <param name="workspace">The workspace to get latest on.</param>
        /// <returns>The output of the get latest.</returns>
        public static string GetLatest(TfsWorkspace workspace)
        {
            var workspaceFolders = GetWorkspaceFolders(workspace).Where(w => w.IsCloaked == false);
            if (workspaceFolders.Count() == 0)
            {
                return "Unable to get, no uncloaked folders\r\n";
            }

            if (workspaceFolders.Count() == 1)
            {
                if (!Directory.Exists(workspaceFolders.First().LocalFolder))
                {
                    Directory.CreateDirectory(workspaceFolders.First().LocalFolder);
                }

                return TFWrapper.Execute("get", workspaceFolders.First().LocalFolder).AsString;
            }

            var results = string.Empty;
            foreach (var workspaceFolder in workspaceFolders)
            {
                Program.OutputInfo(workspaceFolder.LocalFolder);
                if (!Directory.Exists(workspaceFolder.LocalFolder))
                {
                    Directory.CreateDirectory(workspaceFolder.LocalFolder);
                }

                var temp = TFWrapper.Execute("get", workspaceFolder.LocalFolder).AsString;
                if (string.IsNullOrWhiteSpace(temp) && (string.CompareOrdinal(workspaceFolder.LocalFolder, workspaceFolders.Last().LocalFolder) != 0))
                {
                    Program.OutputInfo(string.Empty);
                }

                results = results + temp;
            }

            return results;
        }

        /// <summary>
        /// Returns a collection of the workspaces.
        /// </summary>
        /// <returns>A collection of workspaces.</returns>
        public static IList<TfsWorkspace> GetWorkspaces()
        {
            var items = TFWrapper.Execute("workspaces").AsList;
            var workspaces = new List<TfsWorkspace>();
            var index = 0;
            var collection = string.Empty;
            var workspaceNameEndIndex = -1;
            while (index < items.Count)
            {
                var item = items[index];
                if (!string.IsNullOrWhiteSpace(item))
                {
                    if (item.StartsWith("Collection: ", StringComparison.OrdinalIgnoreCase))
                    {
                        // this line has the colleciton on it
                        collection = item.Substring("Collection: ".Length).Trim();
                        index++;

                        // this line will be the column titles so we can work out how big each one is
                        item = items[index];
                        workspaceNameEndIndex = item.IndexOf(" ", StringComparison.OrdinalIgnoreCase);
                        while (workspaceNameEndIndex < item.Length)
                        {
                            if (item[workspaceNameEndIndex] != ' ')
                            {
                                break;
                            }

                            workspaceNameEndIndex++;
                        }

                        index++;
                    }
                    else
                    {
                        var name = item.Substring(0, (workspaceNameEndIndex != -1) ? workspaceNameEndIndex : item.IndexOf(" ", StringComparison.OrdinalIgnoreCase)).Trim();
                        var workspace = new TfsWorkspace()
                        {
                            Collection = collection,
                            Name = name
                        };
                        workspaces.Add(workspace);
                    }
                }

                index++;
            }

            return workspaces;
        }

        /// <summary>
        /// Returns a collection of workspace folders for the specified <paramref name="workspace"/>.
        /// </summary>
        /// <param name="workspace">The workspace.</param>
        /// <returns>A collection of workspace folders.</returns>
        public static IList<TfsWorkspaceFolder> GetWorkspaceFolders(TfsWorkspace workspace)
        {
            if (workspace == null)
            {
                throw new ArgumentNullException(nameof(workspace));
            }

            var items = TFWrapper.Execute("workfold /collection:\"" + workspace.Collection + "\" /workspace:\"" + workspace.Name + "\"").AsList;
            var folders = new List<TfsWorkspaceFolder>();
            var index = 0;
            while (index < items.Count)
            {
                var item = items[index];
                if (!string.IsNullOrWhiteSpace(item))
                {
                    item = item.Trim();
                    if (
                        (!item.StartsWith("=", StringComparison.OrdinalIgnoreCase))
                        && (!item.StartsWith("Workspace:", StringComparison.OrdinalIgnoreCase))
                        && (!item.StartsWith("Workspace :", StringComparison.OrdinalIgnoreCase))
                        && (!item.StartsWith("Collection:", StringComparison.OrdinalIgnoreCase))
                        && (!item.StartsWith("Collection :", StringComparison.OrdinalIgnoreCase)))
                    {
                        if (item.StartsWith("$", StringComparison.OrdinalIgnoreCase))
                        {
                            var folder = new TfsWorkspaceFolder()
                            {
                                IsCloaked = false,
                                LocalFolder = item.Substring(item.IndexOf(":", StringComparison.OrdinalIgnoreCase) + 1).Trim(),
                                ServerFolder = item.Substring(0, item.IndexOf(":", StringComparison.OrdinalIgnoreCase)).Trim()
                            };
                            folders.Add(folder);
                        }

                        if (item.StartsWith("(cloaked)", StringComparison.OrdinalIgnoreCase))
                        {
                            var folder = new TfsWorkspaceFolder()
                            {
                                IsCloaked = true,
                                LocalFolder = string.Empty,
                                ServerFolder = item.Substring(0, item.IndexOf(":", StringComparison.OrdinalIgnoreCase)).Substring("(cloaked) ".Length).Trim()
                            };
                            folders.Add(folder);
                        }
                    }
                }

                index++;
            }

            return folders;
        }
    }
}
