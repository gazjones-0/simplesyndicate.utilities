﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Globalization;

namespace SimpleSyndicate.TfsUtil.Commands
{
    /// <summary>
    /// Invalid command.
    /// </summary>
    public static class InvalidCommand
    {
        /// <summary>
        /// Executes the invalid command if no command has been executed, and a command has been specified.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Execute(string[] args)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            if (!Program.HasCommandBeenExecuted())
            {
                Program.OutputError(string.Format(CultureInfo.CurrentCulture, "Invalid command \"{0}\"", args[0]));
            }
        }
    }
}
