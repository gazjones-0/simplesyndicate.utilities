﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Linq;

namespace SimpleSyndicate.TfsUtil.Commands
{
    /// <summary>
    /// Utility class for working with commands.
    /// </summary>
    public static class CommandUtility
    {
        /// <summary>
        /// Returns whether the command in the arguments is the same as the <paramref name="command"/> specified.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <param name="command">The command to look for.</param>
        /// <returns>Whether the command in the arguments is the <paramref name="command"/> specified.</returns>
        public static bool IsCommand(string[] args, string command)
        {
            if (string.Compare(CommandUtility.GetCommand(args), command, StringComparison.OrdinalIgnoreCase) == 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns whether the arguments are valid.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns>Whether the arguments are valid.</returns>
        public static bool AreArgumentsValid(string[] args)
        {
            try
            {
                GetArgument(args, "doesntMatter");
                return true;
            }
            catch (InvalidArgumentsException)
            {
                return false;
            }
        }

        /// <summary>
        /// Returns the command specified in the arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns>The command.</returns>
        public static string GetCommand(string[] args)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            if (args.Count() == 0)
            {
                return string.Empty;
            }

            return GetArgumentName(args[0]);
        }

        /// <summary>
        /// Returns the value for the argument with the specified <paramref name="argumentName"/>.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <param name="argumentName">The name of the argument.</param>
        /// <returns>The value of the argument with the specified <paramref name="argumentName"/>.</returns>
        public static string GetArgument(string[] args, string argumentName)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            var parsedArgs = new System.Collections.Generic.Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);

            // the first argument is always the command, so ignore that one
            var position = 1;
            while (position < args.Length)
            {
                var name = GetArgumentName(args[position]);
                position++;
                if (position >= args.Length)
                {
                    throw new InvalidArgumentsException();
                }

                parsedArgs.Add(name, args[position]);
                position++;
            }

            if (parsedArgs.ContainsKey(argumentName))
            {
                return parsedArgs[argumentName];
            }

            return null;
        }

        /// <summary>
        /// Returns the value for the argument with the specified <paramref name="argumentName"/>, or
        /// <paramref name="defaultIfNotSpecified"/> if the argument hasn't been specified; note that
        /// if the argument has been specified, but is blank, blank is returned.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <param name="argumentName">The name of the argument.</param>
        /// <param name="defaultIfNotSpecified">The default to return if the argument has not been specified.</param>
        /// <returns>The value of the argument with the specified <paramref name="argumentName"/>, or <paramref name="defaultIfNotSpecified"/> if it hasn't been specified.</returns>
        public static string GetArgument(string[] args, string argumentName, string defaultIfNotSpecified)
        {
            var argument = GetArgument(args, argumentName);
            if (argument != null)
            {
                return argument;
            }

            return defaultIfNotSpecified;
        }

        /// <summary>
        /// Returns the actual name of the specified argument, after taking any prefixes into account
        /// </summary>
        /// <param name="argument">The argument.</param>
        /// <returns>The argument name.</returns>
        private static string GetArgumentName(string argument)
        {
            if (argument.StartsWith("--", StringComparison.OrdinalIgnoreCase))
            {
                argument = argument.Substring(2);
            }

            if (argument.StartsWith("-", StringComparison.OrdinalIgnoreCase))
            {
                argument = argument.Substring(1);
            }

            if (argument.StartsWith("/", StringComparison.OrdinalIgnoreCase))
            {
                argument = argument.Substring(1);
            }

            return argument;
        }
    }
}
