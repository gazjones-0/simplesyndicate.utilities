﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

namespace SimpleSyndicate.TfsUtil.Commands
{
    /// <summary>
    /// TFS Workspace folder.
    /// </summary>
    public class TfsWorkspaceFolder
    {
        /// <summary>
        /// Gets or sets the server folder.
        /// </summary>
        /// <value>The server folder.</value>
        public string ServerFolder { get; set; }

        /// <summary>
        /// Gets or sets the local folder.
        /// </summary>
        /// <value>The local folder.</value>
        public string LocalFolder { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the folder is cloaked.
        /// </summary>
        /// <value>Whether the folder is cloaked.</value>
        public bool IsCloaked { get; set; }
    }
}
