﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.Reflection;

namespace SimpleSyndicate.TfsUtil.Commands.Version
{
    /// <summary>
    /// Version command.
    /// </summary>
    public static class VersionCommand
    {
        /// <summary>
        /// Gets the current version of the application.
        /// </summary>
        /// <value>Current version.</value>
        public static System.Version CurrentVersion => Assembly.GetExecutingAssembly().GetName().Version;

        /// <summary>
        /// Executes the version command if the arguments specify version as the command.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Execute(string[] args)
        {
            if (CommandUtility.IsCommand(args, "Version"))
            {
                Program.CommandExecuted();
                OutputVersion();
            }
        }

        /// <summary>
        /// Outputs the current version of the application.
        /// </summary>
        public static void OutputVersion() => Program.OutputInfo(VersionCommand.CurrentVersion.ToString(3));
    }
}
