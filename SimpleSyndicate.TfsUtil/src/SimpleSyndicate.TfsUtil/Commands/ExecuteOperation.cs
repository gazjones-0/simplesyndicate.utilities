﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;

namespace SimpleSyndicate.TfsUtil.Commands
{
    /// <summary>
    /// Utility class for executing arbitrary operations with automatic retries.
    /// </summary>
    public static class ExecuteOperation
    {
        /// <summary>
        /// Executes an operation.
        /// </summary>
        /// <param name="operationToExecute"><see cref="Action"/> to execute.</param>
        public static void Execute(Action operationToExecute)
        {
            if (operationToExecute == null)
            {
                throw new ArgumentNullException(nameof(operationToExecute));
            }

            var count = 0;
            while (true)
            {
                try
                {
                    operationToExecute();
                    return;
                }
                catch
                {
                    count++;
                    System.Threading.Thread.Sleep(1000 * 5);
                    if (count > 5)
                    {
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Executes an operation, returning the result.
        /// </summary>
        /// <param name="operationToExecute"><see cref="Func{TResult}"/> to execute.</param>
        /// <returns>The operation result.</returns>
        public static string Execute(Func<string> operationToExecute)
        {
            if (operationToExecute == null)
            {
                throw new ArgumentNullException(nameof(operationToExecute));
            }

            var count = 0;
            while (true)
            {
                try
                {
                    return operationToExecute();
                }
                catch
                {
                    count++;
                    System.Threading.Thread.Sleep(1000 * 5);
                    if (count > 5)
                    {
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Executes an operation, returning the result.
        /// </summary>
        /// <param name="operationToExecute"><see cref="Func{TFWrapper}"/> to execute.</param>
        /// <returns>The operation result.</returns>
        public static TFWrapper Execute(Func<TFWrapper> operationToExecute)
        {
            if (operationToExecute == null)
            {
                throw new ArgumentNullException(nameof(operationToExecute));
            }

            var count = 0;
            while (true)
            {
                try
                {
                    return operationToExecute();
                }
                catch
                {
                    count++;
                    System.Threading.Thread.Sleep(1000 * 5);
                    if (count > 5)
                    {
                        throw;
                    }
                }
            }
        }
    }
}
