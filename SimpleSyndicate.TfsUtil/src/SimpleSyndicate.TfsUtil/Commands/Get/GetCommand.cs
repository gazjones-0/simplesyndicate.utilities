﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;

namespace SimpleSyndicate.TfsUtil.Commands.Get
{
    /// <summary>
    /// Get command.
    /// </summary>
    public static class GetCommand
    {
        /// <summary>
        /// Executes the get command if the arguments specify get as the command.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Execute(string[] args)
        {
            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }

            if (CommandUtility.IsCommand(args, "Get"))
            {
                Program.CommandExecuted();
                if (!TfsClient.Exists())
                {
                    Program.OutputError("tf.exe is not available.");
                    return;
                }

                foreach (var workspace in TfsClient.GetWorkspaces())
                {
                    Program.OutputInfo(workspace.Name + " (" + workspace.Collection + ")");
                    var getoutput = TfsClient.GetLatest(workspace);
                    Program.OutputInfo(getoutput);
                }
            }
        }
    }
}
