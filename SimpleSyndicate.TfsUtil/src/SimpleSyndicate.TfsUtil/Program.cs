﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace SimpleSyndicate.TfsUtil
{
    /// <summary>
    /// The console application.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Whether a command has been executed or not.
        /// </summary>
        private static bool _commandExecuted = false;

        /// <summary>
        /// Application entry point.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            Commands.NoCommand.Execute(args);

            Commands.Get.GetCommand.Execute(args);
            Commands.Help.HelpCommand.Execute(args);
            Commands.Version.VersionCommand.Execute(args);

            // check for invalid command last as it's based on whether a command has been executed
            Commands.InvalidCommand.Execute(args);
        }

        /// <summary>
        /// Flags that a command has been executed.
        /// </summary>
        public static void CommandExecuted() => _commandExecuted = true;

        /// <summary>
        /// Returns whether a command has been executed.
        /// </summary>
        /// <returns>Whether a command has been executed.</returns>
        public static bool HasCommandBeenExecuted() => _commandExecuted;

        /// <summary>
        /// Outputs the specified <paramref name="errorMessage"/>.
        /// </summary>
        /// <param name="errorMessage">The error message.</param>
        public static void OutputError(string errorMessage) => Console.WriteLine("Fatal: " + errorMessage);

        /// <summary>
        /// Outputs the specified <paramref name="message"/>.
        /// </summary>
        /// <param name="message">The message to output.</param>
        public static void OutputInfo(string message) => Console.WriteLine(message);

        /// <summary>
        /// Outputs usage information.
        /// </summary>
        public static void OutputUsage()
        {
            var assembly = typeof(Program).GetTypeInfo().Assembly;
            var rawReadme = string.Empty;
            using (var stream = assembly.GetManifestResourceStream("SimpleSyndicate.TfsUtil.Resources.README.md"))
            {
                using (var reader = new StreamReader(stream, true))
                {
                    rawReadme = reader.ReadToEnd();
                }
            }

            var readme = new List<string>(rawReadme.Split('\n'));
            while (!readme[0].StartsWith("Usage:", StringComparison.OrdinalIgnoreCase))
            {
                readme.RemoveAt(0);
            }

            while (string.IsNullOrWhiteSpace(readme[readme.Count - 1]) || string.Compare(readme[readme.Count - 1].Trim(), "```", StringComparison.OrdinalIgnoreCase) == 0)
            {
                readme.RemoveAt(readme.Count - 1);
            }

            Console.WriteLine(string.Join("\n", readme));
        }
    }
}
