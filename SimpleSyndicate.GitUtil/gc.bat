@echo off
if "%1" == "" (
	set directoryPath=.
) else (
	set directoryPath=%1
)
echo gc'ing repositories in %directoryPath%
pushd .
if exist %directoryPath% (
	pushd .
	cd %directoryPath%
	if exist .git (
		echo gc'ing %directoryPath%
		git gc
	)
	popd
)
for /f "tokens=*" %%G in ('dir /b /a:d /a:-h %directoryPath%') do (
	pushd .
	cd %directoryPath%
	if exist %%G\ (
		cd %%G
		if exist .git (
			echo gc'ing %%G
			git gc
		)
	)
	popd
)
popd
