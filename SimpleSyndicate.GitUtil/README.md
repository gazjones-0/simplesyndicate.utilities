# README #

SimpleSyndicate.GitUtil

### What is this repository for? ###

* Utilities for working with Git.

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.utilities/issues?status=new&status=open

### How do I use it? ###

```
#!text
Usage: fetch <path>
Fetches the repository in the specified path, and any repositories in
sub-folders in the path; if no path is specified, uses current directory.

Usage: pull <path>
Pulls the repository in the specified path, and any repositories in
sub-folders in the path; if no path is specified, uses current directory.

Usage: push <path>
Pushes the repository in the specified path, and any repositories in
sub-folders in the path; if no path is specified, uses current directory.

```
