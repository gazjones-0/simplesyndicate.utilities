@echo off
if "%1" == "" (
	set directoryPath=.
) else (
	set directoryPath=%1
)
echo Fetching repositories in %directoryPath%
pushd .
if exist %directoryPath% (
	pushd .
	cd %directoryPath%
	if exist .git (
		echo Pulling %directoryPath%
		git -c diff.mnemonicprefix=false -c core.quotepath=false pull --all
		git -c diff.mnemonicprefix=false -c core.quotepath=false submodule update --init --recursive
	)
	popd
)
for /f "tokens=*" %%G in ('dir /b /a:d /a:-h %directoryPath%') do (
	pushd .
	cd %directoryPath%
	if exist %%G\ (
		cd %%G
		if exist .git (
			echo Pulling %%G
			git -c diff.mnemonicprefix=false -c core.quotepath=false pull --all
			git -c diff.mnemonicprefix=false -c core.quotepath=false submodule update --init --recursive
		)
	)
	popd
)
popd
