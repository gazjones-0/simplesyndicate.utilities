# README #

SimpleSyndicate.Utilities

### What is this repository for? ###

* Various utilities.

### How do I get started? ###

* See the READMEs in the various sub-folders:
* SimpleSyndicate.GitUtil: https://bitbucket.org/gazooka_g72/simplesyndicate.utilities/src/master/SimpleSyndicate.GitUtil/?at=master
* SimpleSyndicate.TfsUtil: https://bitbucket.org/gazooka_g72/simplesyndicate.utilities/src/master/SimpleSyndicate.TfsUtil/?at=master

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.utilities/issues?status=new&status=open

### How do I use them? ###

* See the READMEs in the various sub-folders.
* SimpleSyndicate.GitUtil: https://bitbucket.org/gazooka_g72/simplesyndicate.utilities/src/master/SimpleSyndicate.GitUtil/?at=master
* SimpleSyndicate.TfsUtil: https://bitbucket.org/gazooka_g72/simplesyndicate.utilities/src/master/SimpleSyndicate.TfsUtil/?at=master
